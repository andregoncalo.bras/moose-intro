package Person;

use Moose;

with 'Printable', 'HasAccount';

has title => (
    is        => 'rw',
    predicate => 'has_title',
    clearer   => 'clear_title',
);

has first_name => (
    is       => 'rw',
    required => 1,
);

has last_name => (
    is       => 'rw',
    required => 1,
);

sub full_name {
    my $self = shift;

    my $full_name = join q{ }, $self->first_name, $self->last_name;
    $full_name .= q[ (] . $self->title . q[)]
        if $self->has_title;

    return $full_name;
}

sub as_string { shift->full_name }

no Moose;

__PACKAGE__->meta->make_immutable;

1;
