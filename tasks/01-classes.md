Create a Person class

A Person has the following attributes:

* first_name - read-write
* last_name - read-write

This class should also have a method named "full_name". This
method should return the first and last name separated by a space
("Jon Smith").

Create an Employee class

The Employee class is a subclass of Person

An Employee has the following attributes:

* title - read-write
* salary - read-write
* ssn - read-only

The Employee class should override the "full_name" method to append the
employee's title in parentheses ("Jon Smith (Programmer)"). You can assume
that the title is always set.
